# Calculate product cost 
The aim of the project is to build an API which calculates cost of the ordered products.

## Steps to use the code:  
1. Set up and activate the virtual environment.
```bash
python3 -m venv venv
source venv/bin/activate
```
2. Install the required modules.
```bash
pip install -r requirements.txt
```  
3. Start the server and worker.
```bash
./server.sh
./workers.sh
```

### Structure of the project:
```bash
calculate_cost_of_product_api/
├── order_api/
│   ├── __init__.py
│   ├── celerifier.py
│   ├── schemas.py           # This file check the schema of input file.
│   ├── views.py             # This file contains all the functions handling the various requests [POST/GET].
│   └── uploads/
│
├── tasks/
│   └── simple_task.py       # This file containes the celery task.
│
├── calculate_cost.py        # This file calulates the final price of the order.
├── config.py
├── README.md
├── requirements.txt
├── wsgi.py
├── server.py 
├── server.sh                # script to start the gunicorn server
└── workers.sh               # script to start the celery workers
```     
         
The application will run on port 5050. The endpoint is http://0.0.0.0:5050/order.
