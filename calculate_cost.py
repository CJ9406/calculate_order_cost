#function to calculate total cost of the order
def cal_cost(data):
    price = 0
    dist = data["distance"]
    dist = data["distance"]/1000
    discount = 0

    #calculating delivery cost based on distance
    if dist <= 10:
        delv_cst = 50
    elif dist >10 and dist <=20:
        delv_cst = 100
    elif dist >20 and dist <=50:
        delv_cst = 500
    else:
        delv_cst = 1000

    #calculating discount if applicable
    if "offer" in data:
        if data["offer"]["offer_type"] == "DELIVERY":
            discount = delv_cst*100
        elif data["offer"]["offer_type"] == "FLAT":
            discount = data["offer"]["offer_val"]

    #calculating price for product without discount
    for order_items in data["order_items"]:
        price = price + order_items["quantity"] * order_items["price"]
        finl_price = price+delv_cst*100

    #taking the minimum discout as final discount
    finl_disc = min(discount,finl_price)

    tot_cost = finl_price - finl_disc
    return {'order_total':tot_cost}
