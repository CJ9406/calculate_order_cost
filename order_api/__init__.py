from flask import Flask
from flask_ipban import IpBan

from order_api.celerifier import make_celery

import os

app = Flask(__name__)

app.config.from_object('config')
app.config['UPLOAD_FOLDER'] = app.root_path + '/' + app.config['RELATIVE_UPLOAD_FOLDER']
app.config['JSON_AS_ASCII'] = False

if not os.path.isdir(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)

app.config['IP_BAN_LIST_COUNT'] = 6
app.config['IP_BAN_LIST_SECONDS'] = 3600

ip_ban = IpBan(app)
ip_ban.load_nuisances()
celery_tasks = make_celery(app)

import order_api.views

