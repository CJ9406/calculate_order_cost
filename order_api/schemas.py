from jsonschema import validate
import json

"""
This Python file defines the schemas for the Claculate Order Cost API 
"""

schemas = {
        '$schema': 'http://json-schema.org/draft-07/schema#',
        'type': 'object',
        'required': ['order_items', 'distance'], 
        'properties':{
            "order_items":{
                "properties":{
                    "name":{
                        "type": "string",  
                        "description": "name of the product that is ordered" 
                        },
                    "price":{
                        "type": "integer", 
                        "description": "price of the ordered product"
                        },
                    "quantity":{
                        "type": "integer", 
                        "description": "quatity of ordered product"
                        }
                    },
                "description": "details of the product ordered"
                },
            "distance":{
                'type': "integer", 
                "minimum": 0, 
                "maximum": 500000, 
                "description": "distance to be travelled in order to deliver product"
                },
            'offer':{   
                "properties":{
                    "offer_type":{
                        "type": "string", 
                        "description": "type of offer to be applied"
                        },
                    "offer_val":{
                        "type": "integer", 
                        "description": "amount of offer"
                        }
                    },
                "description":"offers on product, if any"
                }
            },
       }

def is_schema_valid(json_params):
    try:
        validate(instance=json_params, schema=schemas)
    except Exception as exp:
        return False
    else:
        return True
