from flask import request, url_for, abort, jsonify
import json
from jsonschema import validate
from tasks.simple_task import calculate_cost
from order_api import celery_tasks, app
from order_api.schemas import schemas


@app.route('/order', methods=['GET', 'POST'])
def flask_for_converion():
    if request.method == 'POST':
        if 'data' not in request.files:
            return abort(400, "Missing data file in POST request")
        try:
            data = json.load(request.files['data'])
        except Exception as exp:
            return abort(400, "data is not a json stream/file")
        else:
            try:
                validate(instance=data, schema=schemas)
            except Exception as exp:
                return abort(400, f"Invalid json config. Details: {exp}")
        result = calculate_cost.delay(data) 
        return {'task_url': f"http://{request.host}{url_for('taskstatus', task_id=result.id)}"}
        

@app.route('/schema')
def get_schema():
    return jsonify(schemas)

# revoke pending task
@app.route('/revoke/v0.1/<task_id>')
def revoke_task(task_id):
    celery_tasks.control.revoke(task_id)
    try:
        task = calculate_cost.AsyncResult(task_id)
        response = {
            'task_id': task_id,
            'state': task.state,
            'status': task.info,
        }

    except Exception as exp:
        response = {
            'task_id': task_id,
            'error': str(exp)
        }
    return jsonify(response)


@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = calculate_cost.AsyncResult(task_id)
    print(task.info)
    response = {
        'state': task.state,
        'status': task.info
    }

    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'status': task.info,
            'result': {""}
        }
    elif (task.state == 'PROGRESS') or (task.state == 'SUCCESS'):
        return task.info

    else:
        # something went wrong in the background job
        response = {
            'state': str(task.state),
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)
