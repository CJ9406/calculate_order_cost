from order_api import celery_tasks
from flask import Flask
from calculate_cost import cal_cost

app = Flask(__name__)


@celery_tasks.task(bind=True)
def calculate_cost(self, data):
    value = cal_cost(data)
    return value



